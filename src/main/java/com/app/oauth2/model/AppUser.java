package com.app.oauth2.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class AppUser {

    private Long id;

    @NotNull
    @Size(min = 3, max = 50, message = "The username must be between 3 and 50 characters")
    private String username;

    @NotNull
    @Size(min = 3, max = 30, message = "The password must be between 3 and 30 characters")
    private String password;

    @NotNull
    @Email(message = "The email must match the form \"example@email.com\"")
    private String email;

    private List<String> roleNames;

    public AppUser() {
    }

    public AppUser(Long id, String username, String password, String email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public AppUser(Long id, String username, String password, String email, List<String> roleNames) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.roleNames = roleNames;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getRoleNames() {
        return roleNames;
    }

    public void setRoleNames(List<String> roleNames) {
        this.roleNames = roleNames;
    }
}
