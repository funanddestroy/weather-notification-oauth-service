package com.app.oauth2.service.impl;

import com.app.oauth2.dao.UserRepository;
import com.app.oauth2.model.AppUser;
import com.app.oauth2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void addUser(AppUser appUser) {
        appUser.setPassword(bCryptPasswordEncoder.encode(appUser.getPassword()));
        userRepository.addUser(appUser);
    }

    @Override
    public void deleteUser(AppUser appUser) {
        appUser = userRepository.findByUsername(appUser.getUsername());
        userRepository.deleteUser(appUser);
    }

    @Override
    public List<AppUser> getUserList() {
        return userRepository.getUserList();
    }

    @Override
    public AppUser findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public void addToAdmins(AppUser user) {
        userRepository.addToAdmins(user);
    }

    @Override
    public void removeFromAdmins(AppUser user) {
        userRepository.removeFromAdmins(user);
    }

}
