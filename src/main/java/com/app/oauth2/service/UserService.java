package com.app.oauth2.service;

import com.app.oauth2.model.AppUser;

import java.util.List;

public interface UserService {

    void  addUser(AppUser appUser);

    void deleteUser(AppUser appUser);

    List getUserList();

    AppUser findByUsername(String username);

    void addToAdmins(AppUser user);

    void removeFromAdmins(AppUser user);
}
