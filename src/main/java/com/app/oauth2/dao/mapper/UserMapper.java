package com.app.oauth2.dao.mapper;

import com.app.oauth2.model.AppUser;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<AppUser> {

    @Override
    public AppUser mapRow(ResultSet rs, int rowNum) throws SQLException {

        Long userId = rs.getLong("id");
        String userName = rs.getString("name");
        String password = rs.getString("password");
        String email = rs.getString("email");

        return new AppUser(userId, userName, password, email);
    }

}
