package com.app.oauth2.dao;

import com.app.oauth2.model.AppUser;

import java.util.List;

public interface UserRepository {

    AppUser findByUsername(String username);

    void addUser(AppUser user);

    void deleteUser(AppUser user);

    List getUserList();

    void addToAdmins(AppUser user);

    void removeFromAdmins(AppUser user);
}
