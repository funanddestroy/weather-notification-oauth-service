package com.app.oauth2.dao.impl;

import com.app.oauth2.dao.RoleRepository;
import com.app.oauth2.dao.UserRepository;
import com.app.oauth2.dao.mapper.UserMapper;
import com.app.oauth2.model.AppUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected JdbcOperations jdbcOperations;

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public AppUser findByUsername(String username) {
        String sql = "SELECT * FROM app_user WHERE name = ?;";

        Object[] params = new Object[] { username };
        UserMapper mapper = new UserMapper();
        try {
            AppUser user = jdbcOperations.queryForObject(sql, params, mapper);
            user.setRoleNames(roleRepository.getRoleNamesByUserId(user.getId()));
            return user;
        } catch (EmptyResultDataAccessException e) {
            logger.error("Mapping user error", e);
        }

        return null;
    }

    @Override
    public void addUser(AppUser appUser) {
        String sql = "INSERT INTO app_user (name, password, email) VALUES (?, ?, ?);";

        Object[] params = new Object[] { appUser.getUsername(), appUser.getPassword(), appUser.getEmail() };
        jdbcOperations.update(sql, params);

        AppUser appUserFromDb = findByUsername(appUser.getUsername());
        Long roleId = roleRepository.getIdRole("ROLE_USER");

        sql = "INSERT INTO user_role (user_id, role_id) VALUES (?, ?);";
        params = new Object[] { appUserFromDb.getId(), roleId };
        jdbcOperations.update(sql, params);
    }

    @Override
    public void deleteUser(AppUser appUser) {
        AppUser appUserFromDb = findByUsername(appUser.getUsername());
        String sql = "DELETE FROM user_role WHERE user_id = ?;";

        Object[] params = new Object[] { appUserFromDb.getId() };
        jdbcOperations.update(sql, params);

        sql = "DELETE FROM app_user WHERE name = ?;";

        params = new Object[] { appUser.getUsername() };
        jdbcOperations.update(sql, params);
    }

    @Override
    public List<AppUser> getUserList() {
        String sql = "SELECT * FROM app_user;";

        RowMapper mapper = new UserMapper();
        List<AppUser> userList = jdbcOperations.query(sql, mapper);

        for (AppUser user : userList) {
            user.setRoleNames(roleRepository.getRoleNamesByUserId(user.getId()));
        }

        return userList;
    }

    @Override
    public void addToAdmins(AppUser user) {
        AppUser appUserFromDb = findByUsername(user.getUsername());
        Long roleId = roleRepository.getIdRole("ROLE_ADMIN");

        String sql = "INSERT INTO user_role (user_id, role_id) VALUES (?, ?);";
        Object[] params = new Object[] { appUserFromDb.getId(), roleId };
        jdbcOperations.update(sql, params);
    }

    @Override
    public void removeFromAdmins(AppUser user) {
        AppUser appUserFromDb = findByUsername(user.getUsername());
        Long roleId = roleRepository.getIdRole("ROLE_ADMIN");
        String sql = "DELETE FROM user_role WHERE user_id = ? AND role_id = ?;";

        Object[] params = new Object[] { appUserFromDb.getId(), roleId };
        jdbcOperations.update(sql, params);
    }
}
