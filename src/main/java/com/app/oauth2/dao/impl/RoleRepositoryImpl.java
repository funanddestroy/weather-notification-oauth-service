package com.app.oauth2.dao.impl;

import com.app.oauth2.dao.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    @Autowired
    protected JdbcOperations jdbcOperations;

    @Override
    public List<String> getRoleNamesByUserId(Long userId) {
        String sql = "SELECT app_role.role from user_role, app_role WHERE user_role.role_id = app_role.id AND user_role.user_id = ?;";

        Object[] params = new Object[] { userId };
        return jdbcOperations.queryForList(sql, params, String.class);
    }

    @Override
    public Long getIdRole(String roleName) {
        String sql = "SELECT app_role.id from app_role WHERE app_role.role = ?;";

        Object[] params = new Object[] { roleName };
        return jdbcOperations.queryForObject(sql, params, Long.class);
    }
}
