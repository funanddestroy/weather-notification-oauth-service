package com.app.oauth2.dao;

import java.util.List;

public interface RoleRepository {
    List<String> getRoleNamesByUserId(Long userId);

    Long getIdRole(String roleName);
}
