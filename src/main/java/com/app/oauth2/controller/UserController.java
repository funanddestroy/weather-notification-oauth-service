package com.app.oauth2.controller;

import com.app.oauth2.Oauth2Application;
import com.app.oauth2.model.AppUser;
import com.app.oauth2.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

    public static final Logger logger = LoggerFactory.getLogger(Oauth2Application.class);

    @Autowired
    private UserService userService;


    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public List<AppUser> listAllUsers() {
        List<AppUser> users = userService.getUserList();
        if (users.isEmpty()) {
            return new ArrayList<>();
        }
        return users;
    }

    @PreAuthorize("hasRole('USER')")
    @RequestMapping(value = "/user/{username}", method = RequestMethod.GET)
    public AppUser getUser(@PathVariable("username") String username) {
        logger.info("Fetching User with username {}", username);
        AppUser user = userService.findByUsername(username);
        if (user == null) {
            logger.error("User with username {} not found.", username);
            return new AppUser();
        }
        return user;
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    public void createUser(@RequestBody AppUser user) {
        logger.info("Creating User : {}", user);

        AppUser testUser = userService.findByUsername(user.getUsername());
        if (testUser != null) {
            logger.error("Unable to create. A User with name {} already exist", user.getUsername());
        }

        userService.addUser(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/user/{username}", method = RequestMethod.DELETE)
    public void deleteUser(@PathVariable("username") String username) {
        logger.info("Fetching & Deleting User with username {}", username);

        AppUser user = userService.findByUsername(username);
        if (user == null) {
            logger.error("Unable to delete. User with username {} not found.", username);
        }

        userService.deleteUser(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/admin", method = RequestMethod.POST)
    public void addAdmin(@RequestBody AppUser user) {
        logger.info("Add to Admin : {}", user);

        AppUser testUser = userService.findByUsername(user.getUsername());
        if (testUser != null) {
            logger.error("Unable to add admin. A User with name {} already exist", user.getUsername());
        }

        userService.addToAdmins(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping(value = "/admin/{username}", method = RequestMethod.DELETE)
    public void deleteAdmin(@PathVariable("username") String username) {
        logger.info("Fetching & Deleting Admin with username {}", username);

        AppUser user = userService.findByUsername(username);
        if (user == null) {
            logger.error("Unable to remove admin. User with username {} not found.", username);
        }

        userService.removeFromAdmins(user);
    }

}
